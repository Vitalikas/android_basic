package lt.vitalikas.fragments.data_structures

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class FormState(
    var isValid: Boolean,
    var message: String
) : Parcelable