package lt.vitalikas.lists_recyclerview.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import lt.vitalikas.lists_recyclerview.R
import lt.vitalikas.lists_recyclerview.databinding.ActivityMainBinding
import lt.vitalikas.lists_recyclerview.fragments.FoodListFragment

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if (savedInstanceState == null) {
            addFragment()
        }
    }

    private fun addFragment() {
        supportFragmentManager
            .beginTransaction()
            .add(
                R.id.fragmentContainer,
                FoodListFragment(),
                getString(R.string.tag_food_list_fragment)
            )
            .commit()
    }
}