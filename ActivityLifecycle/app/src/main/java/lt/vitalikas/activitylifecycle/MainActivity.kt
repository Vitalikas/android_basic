package lt.vitalikas.activitylifecycle

import android.content.res.ColorStateList
import android.os.*
import androidx.appcompat.app.AppCompatActivity
import android.util.Log
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.view.isGone
import androidx.core.view.isVisible
import lt.vitalikas.activitylifecycle.BuildConfig.*
import lt.vitalikas.activitylifecycle.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var progressBar: ProgressBar

    private var state = FormState(false, "")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DebugLogger.log(TAG_LIFECYCLE, "onCreate was called")
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        progressBar = getProgressBar()

        binding.buttonLogin.setOnClickListener {
            if (isNotEmptyCredentials()) {
                binding.textCredentials.isVisible = false
                makeOperations()
            } else {
                updateFormState(false, getString(R.string.empty_credentials))
                updateCredentialsState()
            }
        }

        // ANR simulation
        binding.buttonSignUp.setOnClickListener {
            Thread.sleep(20000)
        }
    }

    override fun onStart() {
        super.onStart()
        DebugLogger.log(TAG_LIFECYCLE, "onStart was called")
    }

    override fun onResume() {
        super.onResume()
        DebugLogger.log(TAG_LIFECYCLE, "onResume was called")
    }

    override fun onPause() {
        super.onPause()
        DebugLogger.log(TAG_LIFECYCLE, "onPause was called")
    }

    override fun onStop() {
        super.onStop()
        DebugLogger.log(TAG_LIFECYCLE, "onStop was called")
    }

    override fun onDestroy() {
        super.onDestroy()
        DebugLogger.log(TAG_LIFECYCLE, "onDestroy was called")
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelable(KEY_CREDENTIALS, state)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        state =
            savedInstanceState.getParcelable(KEY_CREDENTIALS)
                ?: throw Exception("KEY not found")
        updateCredentialsState()
    }

    companion object {
        private const val TAG_LIFECYCLE = "test_lifecycle"
        private const val KEY_CREDENTIALS = "credentials"
    }

    private fun updateCredentialsState() {
        binding.textCredentials.text = state.message
    }

    private fun updateFormState(isValid: Boolean, message: String) {
        state.isValid = isValid
        state.message = message
    }

    private fun makeOperations() {
        val views =
            listOf(binding.inputMail, binding.inputPass, binding.checkboxTerms, binding.buttonLogin)

        views.forEach { it.isEnabled = false }

        binding.constraintLayout.addView(progressBar)

        constraintView()

        Handler(Looper.getMainLooper()).postDelayed({
            progressBar.isGone = true

            views.forEach { it.isEnabled = true }

            updateFormState(true, getString(R.string.login_successful, binding.inputMail.text))

            Toast.makeText(
                this@MainActivity,
                state.message,
                Toast.LENGTH_SHORT
            ).show()
        }, 2000)
    }

    private fun constraintView() {
        val params = progressBar.layoutParams as ConstraintLayout.LayoutParams
        params.leftToLeft = binding.guidelineVerticalStart.id
        params.rightToRight = binding.guidelineVerticalEnd.id
        params.topToBottom = binding.checkboxTerms.id
        params.topMargin = 8
        progressBar.requestLayout()
    }

    private fun getProgressBar(): ProgressBar {
        return ProgressBar(this).apply {
            indeterminateTintList =
                ColorStateList.valueOf(ContextCompat.getColor(this@MainActivity, R.color.jalapeno))
        }
    }

    private fun isNotEmptyCredentials() =
        binding.inputMail.text.toString().isNotEmpty() && binding.inputPass.text.toString()
            .isNotEmpty()

    object DebugLogger {
        fun log(tag: String, message: String) {
            if (DEBUG) {
                Log.v(tag, message)
                Log.d(tag, message)
                Log.i(tag, message)
                Log.w(tag, message)
                Log.e(tag, message)
            }
        }
    }
}