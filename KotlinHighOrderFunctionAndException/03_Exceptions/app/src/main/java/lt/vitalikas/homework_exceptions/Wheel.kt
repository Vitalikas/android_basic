package lt.vitalikas.homework_exceptions

import kotlin.Exception

class Wheel {
    class TooHighPressure : Exception("Wheel inflated, but pressure is to high to use.")

    class TooLowPressure : Exception("Wheel inflated, but pressure is to low to use.")

    class IncorrectPressure : Exception("Pressure is incorrect. Operation aborted!")

    var pressure: Double = 0.0
        private set

    fun setPressure(value: Double) {
        if (value < 0.0 || value > 10.0) {
            throw IncorrectPressure()
        } else {
            pressure = value
            checkPressure()
            println("Wheel inflated successfully, pressure is normal to use")
        }
    }

    private fun checkPressure() {
        when {
            pressure < 1.6 -> throw TooLowPressure()
            pressure > 2.5 -> throw TooHighPressure()
        }
    }
}