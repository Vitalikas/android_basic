package lt.vitalikas.lists_recyclerview.fragments

import android.app.Dialog
import android.os.Bundle
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.core.view.isVisible
import androidx.fragment.app.DialogFragment
import lt.vitalikas.lists_recyclerview.R
import lt.vitalikas.lists_recyclerview.databinding.FragmentDataDialogBinding
import lt.vitalikas.lists_recyclerview.models.Food
import java.util.*

class DataDialogFragment : DialogFragment() {
    private val receiver: Receiver?
        get() = parentFragment?.let {
            it as Receiver
        }

    private var _binding: FragmentDataDialogBinding? = null
    private val binding
        get() = _binding!!

    private lateinit var name: String
    private lateinit var link: String
    private lateinit var price: String
    private lateinit var size: Food.Pizza.Size
    private var isVegetarian = false
    private lateinit var type: Food.Sushi.Type

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        _binding = FragmentDataDialogBinding.inflate(layoutInflater)

        with(binding) {
            val sizeGroup = this.pizzaSizeRadioGroup
            val isVeg = this.isVegCheckBox
            val typeGroup = this.sushiTypeRadioGroup
            val foodGroup = this.foodRadioGroup

            foodGroup.setOnCheckedChangeListener { _, checkedIdFood ->
                val foodRadio = dialog?.findViewById<RadioButton>(checkedIdFood)

                if (foodRadio?.text == getString(R.string.pizza)) {
                    listOf(sizeGroup, isVeg).forEach { it.isVisible = true }
                    typeGroup.isVisible = false

                    sizeGroup.setOnCheckedChangeListener { _, checkedIdSize ->
                        val sizeRadio = dialog?.findViewById<RadioButton>(checkedIdSize)
                        size =
                            Food.Pizza.Size.valueOf((sizeRadio?.text as String).uppercase(Locale.getDefault()))
                    }

                    isVeg.setOnCheckedChangeListener { _, isChecked ->
                        isVegetarian = isChecked
                    }
                } else {
                    listOf(sizeGroup, isVeg).forEach { it.isVisible = false }
                    typeGroup.isVisible = true

                    typeGroup.setOnCheckedChangeListener { _, checkedIdType ->
                        val typeRadio = dialog?.findViewById<RadioButton>(checkedIdType)
                        type =
                            Food.Sushi.Type.valueOf((typeRadio?.text as String).uppercase(Locale.getDefault()))
                    }
                }
            }
        }

        return activity?.let {
            val builder = AlertDialog.Builder(it)

            builder
                .setView(binding.root)
                .setPositiveButton(getString(R.string.add)) { _, _ ->

                    with(binding) {
                        name = this.nameEditText.text.toString()
                        link = this.linkEditText.text.toString()
                        price = this.priceEditText.text.toString()
                        val sizeGroup = this.pizzaSizeRadioGroup

                        if (sizeGroup.isVisible) {
                            val pizza = createPizza(
                                name,
                                link,
                                price.toDouble(),
                                size,
                                isVegetarian
                            )
                            receiveFood(pizza)
                        } else {
                            val sushi =
                                createSushi(name, link, price.toDouble(), type)
                            receiveFood(sushi)
                        }
                    }
                }
                .setNegativeButton(getString(R.string.cancel)) { _, _ ->
                    dialog?.cancel()
                }
                .create()
        } ?: error(getString(R.string.error_null_activity))
    }

    override fun onDestroy() {
        super.onDestroy()

        _binding = null
    }

    private fun createPizza(
        name: String,
        link: String,
        price: Double,
        size: Food.Pizza.Size,
        isVeg: Boolean
    ): Food.Pizza {
        return Food.Pizza(name, link, price, size, isVeg)
    }

    private fun createSushi(
        name: String,
        link: String,
        price: Double,
        type: Food.Sushi.Type
    ): Food.Sushi {
        return Food.Sushi(name, link, price, type)
    }

    private fun receiveFood(
        food: Food
    ) {
        receiver?.onFoodReceivedCallback(food)
    }
}