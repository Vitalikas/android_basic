package lt.vitalikas.viewpager_dialogs.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import lt.vitalikas.viewpager_dialogs.models.Type
import lt.vitalikas.viewpager_dialogs.databinding.FragmentPizzaBinding
import lt.vitalikas.viewpager_dialogs.extensions.withFragments

class PizzaFragment : Fragment() {
    private var _binding: FragmentPizzaBinding? = null
    private val binding
        get() = _binding!!

    companion object {
        private const val KEY_TITLE = "title"
        private const val KEY_DESCRIPTION = "description"
        private const val KEY_TYPE = "type"

        fun newInstance(
            @StringRes titleRes: Int,
            @StringRes descriptionRes: Int,
            type: Type
        ): PizzaFragment {
            return PizzaFragment().withFragments {
                putInt(KEY_TITLE, titleRes)
                putInt(KEY_DESCRIPTION, descriptionRes)
                putString(KEY_TYPE, type.name)
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentPizzaBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val title = getString(requireArguments().getInt(KEY_TITLE))
        val description = getString(requireArguments().getInt(KEY_DESCRIPTION))

        binding.textViewTitle.text = title
        binding.textViewDescription.text = description
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}