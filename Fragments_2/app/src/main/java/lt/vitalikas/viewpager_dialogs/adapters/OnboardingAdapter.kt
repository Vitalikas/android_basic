package lt.vitalikas.viewpager_dialogs.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import lt.vitalikas.viewpager_dialogs.models.OnboardingScreen
import lt.vitalikas.viewpager_dialogs.fragments.OnboardingFragment

class OnboardingAdapter(activity: FragmentActivity, private val screens: List<OnboardingScreen>) :
    FragmentStateAdapter(activity) {
    override fun getItemCount(): Int {
        return screens.size
    }

    override fun createFragment(position: Int): Fragment {
        val screen = screens[position]
        return OnboardingFragment.newInstance(
            screen.textRes,
            screen.bgColorRes,
            screen.drawableRes,
            screen.url
        )
    }
}