package lt.vitalikas.equation

import kotlin.math.sqrt

@Throws(ArithmeticException::class)
fun solveEquation(a: Int, b: Int, c: Int):Double {
    // считаем дискриминант
    val d = (b * b) - (4 * a * c)
    println("D = $d")

    return when {
        d > 0 -> {
            // считаем корень x1
            val x1 = (-b + sqrt(d.toDouble())) / (2 * a)
            println("x1 = $x1")
            // считаем корень x2
            val x2 = (-b - sqrt(d.toDouble())) / (2 * a)
            println("x2 = $x2")
            // возвращаем сумму корней
            x1 + x2
        }
        d == 0 -> {
            // считаем корень x
            val x = -b.toDouble() / (2 * a)
            println("x = $x")
            // возвращаем корень
            x
        }
        // если D < 0, "бросаем" ArithmeticException
        else -> throw ArithmeticException()
    }
}

fun main() {
    // "ловим" и обрабатываем ArithmeticException
    val solutionSum = try {
        "Сумма корней = ${solveEquation(a = 4, b = 4, c = 51)}"
    } catch (e: ArithmeticException) {
        "D < 0, корней нет"
    }

    println(solutionSum)
}
