package lt.vitalikas.fragments.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.core.view.children
import androidx.fragment.app.Fragment
import lt.vitalikas.fragments.databinding.FragmentPizzasBinding

class PizzasFragment : Fragment() {
    private var _binding: FragmentPizzasBinding? = null
    private val binding
        get() = _binding!!

    private val navigator: Navigator?
        get() = parentFragment?.let {
            it as? Navigator
        }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentPizzasBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (view as ViewGroup)
            .children
            .mapNotNull { it as? Button }
            .forEach { button ->
                button.setOnClickListener {
                    navigateTo(DetailedInfoFragment.newInstance(getResourceName(button)))
                }
            }
    }

    private fun getResourceName(button: Button): String {
        return button.resources.getResourceName(button.id)
    }

    private fun navigateTo(fragment: Fragment) {
        navigator?.navigateTo(fragment)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}