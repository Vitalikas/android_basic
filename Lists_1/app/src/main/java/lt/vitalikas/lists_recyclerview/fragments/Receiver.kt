package lt.vitalikas.lists_recyclerview.fragments

import lt.vitalikas.lists_recyclerview.models.Food

interface Receiver {
    fun onFoodReceivedCallback(food: Food)
}