package lt.vitalikas.lists_recyclerview.models

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

sealed class Food : Parcelable {

    @Parcelize
    data class Pizza(
        val name: String,
        val pictureLink: String,
        val price: Double,
        val size: Size,
        val isVegetarian: Boolean
    ) : Food() {
        enum class Size {
            SMALL, MEDIUM, LARGE
        }
    }

    @Parcelize
    data class Sushi(
        val name: String,
        val pictureLink: String,
        val price: Double,
        val type: Type
    ) : Food() {
        enum class Type {
            NIGIRI, SASHIMI, MAKI
        }
    }
}