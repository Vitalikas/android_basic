package lt.vitalikas.viewpager_dialogs.activities

import lt.vitalikas.viewpager_dialogs.models.Type

interface Receiver {
    fun onReceiveDataCallback(types: ArrayList<Type>)
}