fun main() {
    val firstName = "Vitalij"
    val lastName = "Kolinko"

    var height = 188
    val weight = 100.5f
    var isChild = height < 150 || weight < 40
    var info = "My name is $firstName, last name is $lastName.\nMy height is ${height}cm " +
            "and weight is ${weight}kg.\nI'm ${if (isChild) "child" else "adult"}."
    println(info)

    height = 100
    isChild = height < 150 || weight < 40
    info = "My name is $firstName, last name is $lastName.\nMy height is ${height}cm " +
            "and weight is ${weight}kg.\nI'm ${if (isChild) "child" else "adult"}."
    println(info)
}
