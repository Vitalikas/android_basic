package lt.vitalikas.homework

open class Room(area: Double) {
    val area: Double = area
    protected open val title: String = "обычная комната"

    fun getDescription() = "название комнаты: $title, площадь комнаты: $area m²"
}