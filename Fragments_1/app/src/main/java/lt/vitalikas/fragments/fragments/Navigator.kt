package lt.vitalikas.fragments.fragments

import androidx.fragment.app.Fragment

interface Navigator {
    fun navigateTo(fragment: Fragment)
}