package lt.vitalikas.intents

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.util.Patterns
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import lt.vitalikas.intents.BuildConfig.DEBUG
import lt.vitalikas.intents.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    companion object {
        private const val TAG_MAIN_LIFECYCLE = "activity_lifecycle"
        private const val CALL_REQUEST_CODE = 1234567890
    }

    private object DebugLogger {
        fun log(tag: String, message: String) {
            if (DEBUG) {
                Log.d(tag, message)
            }
        }
    }

    private val activityResultLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                // no callback for STOCK ACTION_DIAL
                // https://developer.android.com/reference/android/content/Intent#ACTION_CALL
                // Output: nothing.
                showToast("NO RESULT FROM ACTION_DIAL")
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DebugLogger.log(TAG_MAIN_LIFECYCLE, "onCreate|MainActivity|${hashCode()}")
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        binding.buttonCall.setOnClickListener {
            val phoneNumber = binding.editTextNumber.text.toString()
            val isValidNumber =
                Patterns.PHONE.matcher(phoneNumber).matches()
            if (!isValidNumber) {
                showToast("Enter valid phone number")
            } else {
                dialPhoneNumber(phoneNumber)
            }
        }
    }

    private fun dialPhoneNumber(phoneNumber: String) {
        val isDialerPermissionGranted = getDialerPermission()

        if (isDialerPermissionGranted) {
            val intent = Intent(Intent.ACTION_DIAL).apply {
                data = Uri.parse("tel:$phoneNumber")
            }
            if (intent.resolveActivity(packageManager) != null) {
                activityResultLauncher.launch(intent)
//                startActivity(intent)
            } else {
                showToast("No component to handle intent")
            }
        } else {
            requestPermissions()
        }
    }

    private fun requestPermissions() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(Manifest.permission.CALL_PHONE),
            1234567890
        )
    }

    private fun getDialerPermission(): Boolean {
        return ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.CALL_PHONE
        ) == PackageManager.PERMISSION_GRANTED
    }

    override fun onStart() {
        super.onStart()
        DebugLogger.log(TAG_MAIN_LIFECYCLE, "onStart|MainActivity|${hashCode()}")
    }

    override fun onResume() {
        super.onResume()
        DebugLogger.log(TAG_MAIN_LIFECYCLE, "onResume|MainActivity|${hashCode()}")
    }

    override fun onPause() {
        super.onPause()
        DebugLogger.log(TAG_MAIN_LIFECYCLE, "onPause|MainActivity|${hashCode()}")
    }

    override fun onStop() {
        super.onStop()
        DebugLogger.log(TAG_MAIN_LIFECYCLE, "onStop|MainActivity|${hashCode()}")
    }

    override fun onDestroy() {
        super.onDestroy()
        DebugLogger.log(TAG_MAIN_LIFECYCLE, "onDestroy|MainActivity|${hashCode()}")
    }

    private fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }
}