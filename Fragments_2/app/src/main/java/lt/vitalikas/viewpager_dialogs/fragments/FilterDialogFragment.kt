package lt.vitalikas.viewpager_dialogs.fragments

import android.app.Dialog
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import lt.vitalikas.viewpager_dialogs.R
import lt.vitalikas.viewpager_dialogs.activities.Receiver
import lt.vitalikas.viewpager_dialogs.models.Type
import lt.vitalikas.viewpager_dialogs.extensions.withFragments

class FilterDialogFragment : DialogFragment() {
    private val receiver: Receiver?
        get() = activity?.let {
            it as Receiver
        }

    private val types = ArrayList<Type>(Type.values().toList())
    private lateinit var currentTypes: ArrayList<Type>
    private lateinit var currentChecks: BooleanArray
    private val selectedItems = ArrayList<Int>()

    companion object {
        private const val TYPES = "types"

        fun newInstance(types: ArrayList<Type>): FilterDialogFragment {
            return FilterDialogFragment().withFragments {
                putParcelableArrayList(TYPES, types)
            }
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        currentTypes = arguments?.getParcelableArrayList<Type>(TYPES) as ArrayList<Type>

        addItems(currentTypes)

        currentChecks = getChecks(currentTypes)

        val typeNames = Array(types.size) { "" }
        types.forEachIndexed { index, type ->
            typeNames[index] = type.name
        }

        return AlertDialog.Builder(requireContext())
            .setTitle(getString(R.string.select_type))
            .setMultiChoiceItems(typeNames, currentChecks) { _, which, isChecked ->
                if (!isChecked) {
                    selectedItems.remove(which)
                } else {
                    selectedItems.add(which)
                }
            }
            .setPositiveButton(getString(R.string.confirmation_ok)) { _, _ ->
                val currents = createCurrentTypes(selectedItems)

                // PizzasActivity is getting access to lists through interface
                receiveData(currents)
            }
            .setNegativeButton(getString(R.string.confirmation_cancel)) { _, _ ->
                //
            }
            .create()
    }

    private fun getChecks(currentTypes: ArrayList<Type>): BooleanArray {
        currentChecks = BooleanArray(3) { false }

        currentTypes.forEach { currentType ->
            val index = types.indexOf(currentType)
            currentChecks[index] = true
        }

        return currentChecks
    }

    private fun addItems(currentTypes: ArrayList<Type>) {
        currentTypes.forEach { current ->
            val index = types.indexOf(current)
            selectedItems.add(index)
        }
    }

    private fun createCurrentTypes(selections: ArrayList<Int>): ArrayList<Type> {
        currentTypes = ArrayList()

        selections.forEach { selection ->
            currentTypes.add(types[selection])
        }

        return currentTypes
    }

    private fun receiveData(list: ArrayList<Type>) {
        receiver?.onReceiveDataCallback(list)
    }
}