package lt.vitalikas.homework

import lt.vitalikas.homework.Currency.*

sealed class Wallet {
    class VirtualWallet() : Wallet() {
        private var rub: Double = 0.0
        private var usd: Double = 0.0
        private var eur: Double = 0.0

        fun addMoney(currency: Currency, amount: Int) {
            when (currency) {
                RUB -> rub += amount
                USD -> usd += amount
                EUR -> eur += amount
            }
        }

        override fun moneyInUsd(): Double {
            return RUB.convertToUsd(rub) + USD.convertToUsd(usd) + EUR.convertToUsd(eur)
        }

        override fun toString(): String {
            return "VirtualWallet(rub=$rub, usd=$usd, eur=$eur)"
        }
    }

    class RealWallet() : Wallet() {
        private val rub: MutableMap<Int, Int> = mutableMapOf()
        private val usd: MutableMap<Int, Int> = mutableMapOf()
        private val eur: MutableMap<Int, Int> = mutableMapOf()

        fun addMoney(currency: Currency, value: Int, amount: Int) {
            when (currency) {
                RUB -> rub[value] = (rub[value] ?: 0) + amount
                USD -> usd[value] = (usd[value] ?: 0) + amount
                EUR -> eur[value] = (eur[value] ?: 0) + amount
            }
        }

        override fun moneyInUsd(): Double {
            return (rub.map { it.key * it.value * CurrencyConverter.rubToUsd } +
                    usd.map { it.key * it.value * CurrencyConverter.usdToUsd } +
                    eur.map { it.key * it.value * CurrencyConverter.eurToUsd }).sum()
        }

        override fun toString(): String {
            return "RealWallet(rub=$rub, usd=$usd, eur=$eur)"
        }
    }

    abstract fun moneyInUsd(): Double
}