package lt.vitalikas.homework_codeconventions

import lt.vitalikas.homework_codeconventions.Currency.Companion.nationalCurrency
import lt.vitalikas.homework_codeconventions.Currency.EUR
import lt.vitalikas.homework_codeconventions.Currency.RUB
import lt.vitalikas.homework_codeconventions.Currency.USD

enum class Currency {
    RUB, USD, EUR;

    companion object {
        val nationalCurrency = EUR
    }
}

val Currency.isNational: Boolean
    get() = this == nationalCurrency

object CurrencyConverter {
    const val rubToUsd = 0.013
    const val usdToUsd = 1.000
    const val eurToUsd = 1.210
}

fun Currency.convertToUsd(amount: Double): Double {
    return when (this) {
        RUB -> amount * CurrencyConverter.rubToUsd
        USD -> amount * CurrencyConverter.usdToUsd
        EUR -> amount * CurrencyConverter.eurToUsd
    }
}
