package lt.vitalikas.collections

fun createNumberMap(numberSet: Set<String>): Map<String, String> {
    val numberMap = mutableMapOf<String, String>()
    numberSet.forEach { number ->
        run {
            print("Введите имя человека с номером телефона $number: ")
            val name = readLine() ?: "default name"
            numberMap[number] = name
        }
    }
    return numberMap
}

fun createNumberList(amount: Int): List<String> {
    val numberList = mutableListOf<String>()
    repeat(amount) {
        print("Enter phone number: ")
        val number = readLine() ?: "default number"
        numberList.add(number)
    }
    return numberList
}

fun readInput(): Int {
    return readLine()?.toIntOrNull() ?: readInput()
}

fun main() {
    print("Amount of entries: ")
    var amount = readInput()
    while (amount <= 0) {
        println("Wrong input, amount must be positive! Please try again. Amount of entries: ")
        amount = readInput()
    }

    val list = createNumberList(amount)
    list
        .filter { it.startsWith("+7") }
        .forEach { println(it) }

    val uniqNumberList = list.toSet()
    println(uniqNumberList.size)

    val sum = list.sumBy { it.length }
    println(sum)

    val numberMap = createNumberMap(uniqNumberList)
    numberMap.forEach { (number, name) ->
        println("Человек: $name. Номер телефона: $number")
    }
}