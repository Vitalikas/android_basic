package lt.vitalikas.fragments.fragments

import android.content.res.ColorStateList
import android.os.*
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.view.isGone
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import lt.vitalikas.fragments.data_structures.FormState
import lt.vitalikas.fragments.R
import lt.vitalikas.fragments.databinding.FragmentLoginBinding
import lt.vitalikas.fragments.utils.DebugLogger

class LoginFragment : Fragment() {
    private var _binding: FragmentLoginBinding? = null
    private val binding
        get() = _binding!!

    private var _loadingAnimation: ProgressBar? = null
    private val loadingAnimation
        get() = _loadingAnimation!!

    private lateinit var state: FormState

    private val navigator: Navigator?
        get() = activity?.let {
            it as? Navigator
        }

    companion object {
        private const val KEY_CREDENTIALS = "credentials"
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        _loadingAnimation = getProgressBar()

        state = savedInstanceState?.getParcelable(KEY_CREDENTIALS) ?: FormState(false, "")
        updateCredentialsState()

        binding.buttonLogin.setOnClickListener {
            if (isNotEmptyCredentials()) {
                val isValidEmail =
                    Patterns.EMAIL_ADDRESS.matcher(binding.inputMail.text.toString()).matches()
                if (!isValidEmail) {
                    binding.textCredentials.text = ""
                    showToast("Please enter valid email address")
                } else {
                    state = FormState(
                        true,
                        getString(R.string.login_successful, binding.inputMail.text)
                    )
                    binding.textCredentials.isVisible = false
                    makeOperations()
                }
            } else {
                state = FormState(false, getString(R.string.empty_credentials))
                DebugLogger.log("login_fragment", state.message)
                updateCredentialsState()
            }
        }

        // ANR simulation
        binding.buttonSignUp.setOnClickListener {
            Thread.sleep(20000)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        outState.putParcelable(KEY_CREDENTIALS, state)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
        _loadingAnimation = null
    }

    private fun updateCredentialsState() {
        if (!state.isValid) {
            binding.textCredentials.text = state.message
        }
    }

    private fun makeOperations() {
        val views =
            listOf(binding.inputMail, binding.inputPass, binding.checkboxTerms, binding.buttonLogin)

        views.forEach { it.isEnabled = false }

        binding.constraintLayout.addView(loadingAnimation)

        constraintView()

        Handler(Looper.getMainLooper()).postDelayed({
            loadingAnimation.isGone = true

            views.forEach { it.isEnabled = true }

            showToast(state.message)

            navigateTo(MainFragment())
        }, 2000)
    }

    private fun navigateTo(fragment: Fragment) {
        navigator?.navigateTo(fragment)
    }

    private fun constraintView() {
        val params = loadingAnimation.layoutParams as ConstraintLayout.LayoutParams
        params.leftToLeft = binding.guidelineVerticalStart.id
        params.rightToRight = binding.guidelineVerticalEnd.id
        params.topToBottom = binding.checkboxTerms.id
        params.topMargin = 8
        loadingAnimation.requestLayout()
    }

    private fun getProgressBar(): ProgressBar {
        return ProgressBar(context).apply {
            indeterminateTintList =
                ColorStateList.valueOf(ContextCompat.getColor(context, R.color.jalapeno))
        }
    }

    private fun isNotEmptyCredentials() =
        binding.inputMail.text.toString().isNotEmpty() && binding.inputPass.text.toString()
            .isNotEmpty()

    private fun showToast(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }
}