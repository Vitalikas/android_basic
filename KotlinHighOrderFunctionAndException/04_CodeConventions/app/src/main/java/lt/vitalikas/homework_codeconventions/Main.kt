package lt.vitalikas.homework_codeconventions

import lt.vitalikas.homework_codeconventions.Currency.EUR
import lt.vitalikas.homework_codeconventions.Currency.RUB
import lt.vitalikas.homework_codeconventions.Currency.USD

fun main() {
    println(EUR.isNational)

    println(RUB.convertToUsd(800.00))

    val virtualWallet = Wallet.VirtualWallet(0.0, 0.0, 0.0)
    virtualWallet.addMoney(RUB, 800)
    virtualWallet.addMoney(USD, 20)
    virtualWallet.addMoney(EUR, 60)
    println(virtualWallet)
    println("${virtualWallet.moneyInUsd()}$")

    val realWallet = Wallet.RealWallet(
        mutableMapOf(),
        mutableMapOf(10 to 1),
        mutableMapOf(20 to 3)
    )
    realWallet.addMoney(RUB, 100, 8)
    realWallet.addMoney(USD, 10, 1)
    println(realWallet)
    println("${realWallet.moneyInUsd()}$")
}
