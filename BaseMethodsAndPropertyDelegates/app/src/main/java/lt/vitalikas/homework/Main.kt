package lt.vitalikas.homework

fun main() {
    val person1 = Person(175, 80, "Name")
    val person2 = Person(175, 80, "Name")

    val persons = mutableSetOf<Person>()
    persons.addAll(listOf(person1, person2))

    println(persons.size)

    persons.add(Person(180, 77, "Name"))
    persons.forEach { println(it) }

    persons.forEach { person -> repeat((1..3).random()) { person.buyPet() } }
    persons.forEach { println(it) }
}