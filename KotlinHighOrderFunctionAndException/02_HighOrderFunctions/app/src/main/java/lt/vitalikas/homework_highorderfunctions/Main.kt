package lt.vitalikas.homework_highorderfunctions

fun main() {
    val queue = Queue<Any>()
    queue.enqueue(5)
    queue.enqueue(2.0)
    queue.enqueue(3_000_000_000L)
    queue.enqueue("string")
    queue.enqueue(true)
    queue.enqueue(153)
    queue.enqueue(hashMapOf(1 to "one"))
    println(queue)

    // lambda
    println(queue.filter { it is Int })

    // method reference
    println(queue.filter(::filterInts))
}

fun <T> filterInts(item: T): Boolean {
    return item is Int
}