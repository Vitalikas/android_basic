package lt.vitalikas.viewpager_dialogs.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class MyBottomSheetDialogFragment : BottomSheetDialogFragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // instead of inflater use custom layout from R.layout.fragment_dialog_bottomsheet
        return super.onCreateView(inflater, container, savedInstanceState)
    }
}