package lt.vitalikas.viewpager_dialogs.utils

import android.util.Log

object DebugLogger {
    fun log(tag: String, message: String) {
        Log.d(tag, message)
    }
}