package lt.vitalikas.homework

fun main() {
    val room = Room(80.0)
    println(room.getDescription())

    val childrenRoom = ChildrenRoom(10.5)
    println(childrenRoom.getDescription())
    childrenRoom.title = "кабинет"
    println(childrenRoom.title)

    val freeRoom = FreeRoom(104.5, "комната свободного назначения")
    println(freeRoom.title)
    freeRoom.title = "свободная комната"
    println(freeRoom.title)

    val bedroom = Bedroom(40.0)
    val livingRoom = LivingRoom(100.0)
    val kitchen = Kitchen(30.4)
    val bathroom = Bathroom(20.1)

    println(
        "Жилище моей мечты состоит из таких комнат: " +
                "\n- ${bedroom.getDescription()}," +
                "\n- ${livingRoom.getDescription()}," +
                "\n- ${kitchen.getDescription()}," +
                "\n- ${bathroom.getDescription()}."
    )
}