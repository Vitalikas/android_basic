package lt.vitalikas.homework_generics

fun <T : Number> filterEvenNumbers(list: List<T>): List<T> = list.filter { it.toDouble() % 2 == 0.0 }

fun main() {
    println(filterEvenNumbers(listOf(1.2, 2, 5, 8.0, 1, 2, -8, 0, -4.7, 2.8, 3.33)))

    val queue = Queue<Any>()
    queue.enqueue(5)
    queue.enqueue(2.0)
    queue.enqueue(3_000_000_000L)
    queue.enqueue("string")
    queue.enqueue(true)
    queue.enqueue(hashMapOf(1 to "one"))
    println(queue)

    val element = queue.dequeue()
    println(element)
    println(queue)

    // you can do that. Result<T, R> class is covariant by T
    val result1: Result<Number, String> = getResult()
    val result2: Result<Any, String> = getResult()
//     you can't do that. Result<T, R> class invariant by R
//    val result3: Result<Int, CharSequence> = getResult()
//    val result4: Result<Int, Any> = getResult()
    println(result1)
}