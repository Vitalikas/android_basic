package lt.vitalikas.homework_highorderfunctions

class Queue<T> {
    private val list = mutableListOf<T>()

    fun enqueue(item: T) = list.add(item)

    fun dequeue(): T? = list.removeFirstOrNull()

    fun filter(predicate: (T) -> Boolean): Queue<T> {
        val queue = Queue<T>()
        list.forEach {
            if (predicate(it)) {
                queue.enqueue(it)
            }
        }
        return queue
    }

    override fun toString(): String {
        return "Queue($list)"
    }
}