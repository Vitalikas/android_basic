package lt.vitalikas.viewpager_dialogs.models

import android.os.Parcelable
import androidx.annotation.StringRes

data class Pizza(
    @StringRes val titleRes: Int,
    @StringRes val descriptionRes: Int,
    val type: Type
)

@kotlinx.parcelize.Parcelize
enum class Type : Parcelable {
    CHEESE, VEGETARIAN, MEAT
}