package lt.vitalikas.viewpager_dialogs.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import lt.vitalikas.viewpager_dialogs.R
import lt.vitalikas.viewpager_dialogs.adapters.PizzasAdapter
import lt.vitalikas.viewpager_dialogs.models.Pizza
import lt.vitalikas.viewpager_dialogs.models.Type
import lt.vitalikas.viewpager_dialogs.databinding.ActivityPizzasBinding
import lt.vitalikas.viewpager_dialogs.fragments.FilterDialogFragment

class PizzasActivity : AppCompatActivity(), Receiver {
    private lateinit var binding: ActivityPizzasBinding

    private val pizzas = listOf(
        Pizza(R.string.pizza_1, R.string.pizza_1_description, Type.CHEESE),
        Pizza(R.string.pizza_2, R.string.pizza_2_description, Type.CHEESE),
        Pizza(R.string.pizza_3, R.string.pizza_3_description, Type.CHEESE),
        Pizza(R.string.pizza_4, R.string.pizza_4_description, Type.CHEESE),
        Pizza(R.string.pizza_5, R.string.pizza_5_description, Type.CHEESE),
        Pizza(R.string.pizza_6, R.string.pizza_6_description, Type.VEGETARIAN),
        Pizza(R.string.pizza_7, R.string.pizza_7_description, Type.VEGETARIAN),
        Pizza(R.string.pizza_8, R.string.pizza_8_description, Type.VEGETARIAN),
        Pizza(R.string.pizza_9, R.string.pizza_9_description, Type.VEGETARIAN),
        Pizza(R.string.pizza_10, R.string.pizza_10_description, Type.VEGETARIAN),
        Pizza(R.string.pizza_11, R.string.pizza_11_description, Type.MEAT),
        Pizza(R.string.pizza_12, R.string.pizza_12_description, Type.MEAT),
        Pizza(R.string.pizza_13, R.string.pizza_13_description, Type.MEAT),
        Pizza(R.string.pizza_14, R.string.pizza_14_description, Type.MEAT),
        Pizza(R.string.pizza_15, R.string.pizza_15_description, Type.MEAT)
    )

    private lateinit var filteredPizzas: ArrayList<Pizza>

    private var currentTypes = ArrayList<Type>(Type.values().toList())

    private var _pager: ViewPager2? = null
    private val pager
        get() = _pager!!

    private var _tab: TabLayout? = null
    private val tab
        get() = _tab!!

    companion object {
        private const val TYPES = "types"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityPizzasBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        _pager = binding.viewPager
        _tab = binding.tabLayout

        createTabs(pizzas)

        setPageTransformer()

        handleTabColor()

        binding.buttonFilterByType.setOnClickListener {
            showDialogFragment()
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        outState.putParcelableArrayList(TYPES, currentTypes)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)

        currentTypes = savedInstanceState.getParcelableArrayList(TYPES)
            ?: throw Exception("Unexpected error occurred on getting types from bundle")

        if (currentTypes.isNotEmpty()) {
            createCurrentTabs(currentTypes)
        } else {
            createTabs(pizzas)
        }
    }

    private fun showDialogFragment() {
        FilterDialogFragment.newInstance(currentTypes).show(supportFragmentManager, "dialog")
    }

    private fun createTabs(pizzas: List<Pizza>) {
        val adapter = PizzasAdapter(this, pizzas)
        pager.adapter = adapter

        TabLayoutMediator(tab, pager) { tab, position ->
            tab.setText(pizzas[position].titleRes)
            tab.setIcon(R.drawable.ic_pizza)
        }.attach()
    }

    private fun setPageTransformer() {
        // animation transform
        pager.setPageTransformer { page, position ->
            when {
                position < -1 || position > 1 -> {
                    page.alpha = 0f
                }
                position <= 0 -> {
                    page.alpha = 1 + position
                }
                position <= 1 -> {
                    page.alpha = 1 - position
                }
            }
        }
    }

    private fun handleTabColor() {
        val selectedTabColor = ContextCompat.getColor(this@PizzasActivity, R.color.jalapeno)
        val unselectedTabColor = ContextCompat.getColor(this@PizzasActivity, R.color.white)

        tab.getTabAt(0)?.icon?.setTint(selectedTabColor)

        tab.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                tab?.icon?.setTint(selectedTabColor)
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
                tab?.icon?.setTint(unselectedTabColor)
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
            }
        })
    }

    override fun onReceiveDataCallback(types: ArrayList<Type>) {
        currentTypes = types

        createCurrentTabs(currentTypes)
    }

    private fun createCurrentTabs(types: List<Type>) {
        val filteredPizzas = getFilteredPizzasByType(types)
        createTabs(filteredPizzas)
    }

    private fun getFilteredPizzasByType(types: List<Type>): ArrayList<Pizza> {
        filteredPizzas = ArrayList()

        types.forEach { type ->
            pizzas.forEach {
                if (it.type == type) {
                    filteredPizzas.add(it)
                }
            }
        }

        return filteredPizzas
    }

    private fun hideDialogFragment() {
        supportFragmentManager.findFragmentByTag("dialog")
            ?.let { it as FilterDialogFragment }
            ?.dismiss()
    }

//    private fun createDialog() {
//        val types = arrayOf(
//            Type.CHEESE.name,
//            Type.VEGETARIAN.name,
//            Type.MEAT.name,
//            Type.ALL.name
//        )
//        val selectedItems = ArrayList<Int>()
//        sortedPizzas = ArrayList()
//
//        AlertDialog.Builder(this)
//            .setTitle("Select type")
//            .setMultiChoiceItems(types, null) { _, which, isChecked ->
//                if (isChecked) {
//                    selectedItems.add(which)
//                } else if (selectedItems.contains(which)) {
//                    selectedItems.remove(Integer.valueOf(which))
//                }
//            }
//            .setPositiveButton("OK") { _, _ ->
//                selectedItems.forEach { item ->
//                    pizzas.forEach {
//                        if (it.type.name == types[item]) {
//                            (sortedPizzas as ArrayList<Pizza>).add(it)
//                        }
//                    }
//                }
//
//                if (selectedItems.size == 0) {
//                    setPagerAdapter(pizzas)
//                    createTabs(pizzas)
//                } else {
//                    setPagerAdapter(sortedPizzas)
//                    createTabs(sortedPizzas)
//                }
//            }
//            .setNegativeButton("CANCEL") { dialog, id ->
//                //
//            }

//            // SINGLE ITEM CHOICE
//            .setItems(types) { _, which ->
//                val sortedPizzas = pizzas.filter {
//                    it.type.name == types[which]
//                }
//
//                if (types[which] == types[types.lastIndex]) {
//                    setPagerAdapter(pizzas)
//                    createTabs(pizzas)
//                } else {
//                    setPagerAdapter(sortedPizzas)
//                    createTabs(sortedPizzas)
//                }
//            }

//            .show()
//    }
}