package lt.vitalikas.toolbar

import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.annotation.RequiresApi
import lt.vitalikas.toolbar.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    private val pizzas = (0..100).toList().map {
        "${
            listOf(
                "Pepperoni",
                "Banana",
                "Mushroom"
            ).random()
        }$it"
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        pizzas.joinToString().also {
            binding.textViewPizzas.text = it
        }

        initToolbar()
    }

    private fun initToolbar() {
        with(binding.toolbar) {
            title = getString(R.string.app_name)

            inflateMenu(R.menu.menu_toolbar)

            setNavigationOnClickListener {
                Toast.makeText(this@MainActivity, "Menu clicked", Toast.LENGTH_SHORT).show()
            }

            setOnMenuItemClickListener {
                when (it.itemId) {
                    R.id.toolbar_menu_mail -> {
                        showToast("${it.title} clicked")
                        true
                    }
                    R.id.toolbar_menu_info -> {
                        showToast("${it.title} clicked")
                        true
                    }
                    else -> false
                }
            }

            val searchItem = menu.findItem(R.id.toolbar_menu_search)
            searchItem.setOnActionExpandListener(object : MenuItem.OnActionExpandListener {
                override fun onMenuItemActionExpand(item: MenuItem?): Boolean {
                    showToast("search expanded")
                    return true
                }

                override fun onMenuItemActionCollapse(item: MenuItem?): Boolean {
                    showToast("search collapsed")
                    return true
                }
            })
            (searchItem.actionView as androidx.appcompat.widget.SearchView).setOnQueryTextListener(
                object : androidx.appcompat.widget.SearchView.OnQueryTextListener {
                    override fun onQueryTextSubmit(query: String?): Boolean {
                        return true
                    }

                    override fun onQueryTextChange(newText: String?): Boolean {
                        pizzas.filter { it.contains(newText ?: "", true) }
                            .joinToString()
                            .also {
                                binding.textViewPizzas.text = it
                            }
                        return true
                    }
                })
        }
    }

    private fun showToast(text: String) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show()
    }
}