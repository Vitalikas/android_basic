package lt.vitalikas.login

import android.content.res.ColorStateList
import android.os.*
import androidx.appcompat.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.isGone
import lt.vitalikas.login.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var progressBar: ProgressBar

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        progressBar = getProgressBar()

        binding.groupInputs.referencedIds.forEach {
            findViewById<TextView>(it).addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(
                    s: CharSequence?,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    toggleLoginButton()
                }

                override fun afterTextChanged(s: Editable?) {
                    toggleLoginButton()
                }
            })
        }

        binding.checkboxTerms.setOnCheckedChangeListener { _, _ ->
            toggleLoginButton()
        }

        binding.buttonLogin.setOnClickListener { makeOperations() }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun makeOperations() {
        val views =
            listOf(binding.inputMail, binding.inputPass, binding.checkboxTerms, binding.buttonLogin)

        views.forEach { it.isEnabled = false }

        binding.constraintLayout.addView(progressBar)

        constraintView()

        Handler(Looper.getMainLooper()).postDelayed({
            progressBar.isGone = true

            views.forEach { it.isEnabled = true }

            Toast.makeText(
                this@MainActivity,
                resources.getString(R.string.login_successful, binding.inputMail.text),
                Toast.LENGTH_SHORT
            ).show()
        }, 2000)
    }

    private fun constraintView() {
        val params = progressBar.layoutParams as ConstraintLayout.LayoutParams
        params.leftToLeft = binding.guidelineStart.id
        params.rightToRight = binding.guidelineEnd.id
        params.topToBottom = binding.buttonLogin.id
        params.topMargin = 64
        progressBar.requestLayout()
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun getProgressBar(): ProgressBar {
        return ProgressBar(this).apply {
            indeterminateTintList = ColorStateList.valueOf(getColor(R.color.jalapeno))
        }
    }

    private fun toggleLoginButton() {
        binding.buttonLogin.isEnabled = binding.inputMail.text.isNotEmpty() &&
                binding.inputPass.text.isNotEmpty() && binding.checkboxTerms.isChecked
    }
}