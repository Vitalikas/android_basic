package lt.vitalikas.fragments.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import lt.vitalikas.fragments.databinding.FragmentDetailedInfoBinding
import lt.vitalikas.fragments.extensions.withFragments

class DetailedInfoFragment : Fragment() {
    private var _binding: FragmentDetailedInfoBinding? = null
    private val binding
        get() = _binding!!

    companion object {
        private const val KEY_TEXT = "key_text"

        fun newInstance(text: String): DetailedInfoFragment {
            return DetailedInfoFragment().withFragments {
                putString(KEY_TEXT, text)
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentDetailedInfoBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.textViewDetails.text = requireArguments().getString(KEY_TEXT)

        binding.buttonBack.setOnClickListener {
            goBack()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun goBack() {
        // custom logic of onBackPressed()
    }
}