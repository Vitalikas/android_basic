package lt.vitalikas.fragments.utils

import android.util.Log

object DebugLogger {
    fun log(tag: String, message: String) {
        Log.d(tag, message)
    }
}