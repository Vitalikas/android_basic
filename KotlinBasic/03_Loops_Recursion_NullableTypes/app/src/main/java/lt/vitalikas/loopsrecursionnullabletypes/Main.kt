package lt.vitalikas.loopsrecursionnullabletypes

import kotlin.math.abs

tailrec fun calculateGcd(number1: Int, number2: Int): Int {
    if (number2 == 0) return abs(number1)
    return calculateGcd(number2, number1 % number2)
}

fun readInput(): Int {
    return readLine()?.toIntOrNull() ?: readInput()
}

fun calculatePositiveNumberCountAndSum(amount: Int): Int {
    var sum = 0
    var positiveNumberCount = 0
    repeat(amount) {
        print("Enter number: ")
        val number = readInput()
        if (number > 0) positiveNumberCount++
        sum += number
    }
    println("Positive number count: $positiveNumberCount")
    return sum
}

fun main() {
    print("Amount of entries: ")
    var amount = readInput()
    while (amount <= 0) {
        println("Wrong input, amount must be positive! Please try again. Amount of entries: ")
        amount = readInput()
    }

    val sum = calculatePositiveNumberCountAndSum(amount)

    print("Enter the number to calculate GCD of this number and $sum: ")
    val number = readInput()
    println("GCD = ${calculateGcd(number, sum)}")
}
