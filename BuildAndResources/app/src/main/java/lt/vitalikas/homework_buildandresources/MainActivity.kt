package lt.vitalikas.homework_buildandresources

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        findViewById<TextView>(R.id.appInfo).text = getInfo()
    }

    private fun getInfo() = "${getString(R.string.flavor)} = ${BuildConfig.FLAVOR}" +
            "\n${getString(R.string.build)} = ${BuildConfig.BUILD_TYPE}" +
            "\n${getString(R.string.version_code)} = ${BuildConfig.VERSION_CODE}" +
            "\n${getString(R.string.version_name)} = ${BuildConfig.VERSION_NAME}" +
            "\n${getString(R.string.app_id)} = ${BuildConfig.APPLICATION_ID}"
}