package lt.vitalikas.homework_codeconventions

import lt.vitalikas.homework_codeconventions.Currency.EUR
import lt.vitalikas.homework_codeconventions.Currency.RUB
import lt.vitalikas.homework_codeconventions.Currency.USD

sealed class Wallet {
    class VirtualWallet(
        private var rub: Double,
        private var usd: Double,
        private var eur: Double
    ) : Wallet() {
        fun addMoney(currency: Currency, amount: Int) {
            when (currency) {
                RUB -> rub += amount
                USD -> usd += amount
                EUR -> eur += amount
            }
        }

        override fun moneyInUsd(): Double {
            return RUB.convertToUsd(rub) + USD.convertToUsd(usd) + EUR.convertToUsd(eur)
        }

        override fun toString(): String {
            return "VirtualWallet(rub=$rub, usd=$usd, eur=$eur)"
        }
    }

    class RealWallet(
        private val rub: MutableMap<Int, Int>,
        private val usd: MutableMap<Int, Int>,
        private val eur: MutableMap<Int, Int>
    ) : Wallet() {
        fun addMoney(currency: Currency, value: Int, amount: Int) {
            when (currency) {
                RUB -> if (rub.containsKey(value)) {
                    rub[value] = rub.getValue(value) + amount
                } else {
                    rub[value] = amount
                }
                USD -> if (usd.containsKey(value)) {
                    usd[value] = usd.getValue(value) + amount
                } else {
                    usd[value] = amount
                }
                EUR -> if (eur.containsKey(value)) {
                    eur[value] = eur.getValue(value) + amount
                } else {
                    eur[value] = amount
                }
            }
        }

        override fun moneyInUsd(): Double {
            return (
                rub.map { it.key * it.value * CurrencyConverter.rubToUsd } +
                    usd.map { it.key * it.value * CurrencyConverter.usdToUsd } +
                    eur.map { it.key * it.value * CurrencyConverter.eurToUsd }
                ).sum()
        }

        override fun toString(): String {
            return "RealWallet(rub=$rub, usd=$usd, eur=$eur)"
        }
    }

    abstract fun moneyInUsd(): Double
}
