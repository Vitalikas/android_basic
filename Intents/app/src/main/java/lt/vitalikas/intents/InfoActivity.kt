package lt.vitalikas.intents

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import lt.vitalikas.intents.databinding.ActivityInfoBinding

class InfoActivity : AppCompatActivity() {
    private lateinit var binding: ActivityInfoBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityInfoBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        handleIntentData()
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        handleIntentData()
    }

    private fun getFullPath(): String {
        var protocol: String?
        var host: String?
        var path: String?

        intent.data?.scheme.let {
            protocol = it
        }
        intent.data?.host.let {
            host = it
        }
        intent.data?.path.let {
            path = it
        }

        return "$protocol://$host$path"
    }

    private fun handleIntentData() {
        binding.textViewInfo.text = getFullPath()
    }
}