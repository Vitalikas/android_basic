package lt.vitalikas.homework

import lt.vitalikas.homework.Currency.*
import lt.vitalikas.homework.Currency.Companion.nationalCurrency

enum class Currency {
    RUB, USD, EUR;

    companion object {
        val nationalCurrency = EUR
    }
}

val Currency.isNational: Boolean
    get() = this == nationalCurrency

object CurrencyConverter {
    const val rubToUsd = 0.013
    const val usdToUsd = 1.000
    const val eurToUsd = 1.210
}


fun Currency.convertToUsd(amount: Double): Double {
    return when (this) {
        RUB -> amount * CurrencyConverter.rubToUsd
        USD -> amount * CurrencyConverter.usdToUsd
        EUR -> amount * CurrencyConverter.eurToUsd
    }
}