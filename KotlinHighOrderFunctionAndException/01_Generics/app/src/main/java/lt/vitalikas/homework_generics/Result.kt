package lt.vitalikas.homework_generics

import kotlin.random.Random

sealed class Result<out T, R> {
    data class Success<T, R>(val obj: T) : Result<T, R>()

    data class Error<T, R>(val obj: R) : Result<T, R>()
}

fun getResult(): Result<Int, String> {
    return if (Random.nextBoolean()) Result.Success(1) else Result.Error("error")
}