package lt.vitalikas.homework

import lt.vitalikas.homework.Currency.*

fun main() {
    println(EUR.isNational)

    println(RUB.convertToUsd(800.00))

    val virtualWallet = Wallet.VirtualWallet()
    virtualWallet.addMoney(RUB, 800)
    virtualWallet.addMoney(USD, 20)
    virtualWallet.addMoney(EUR, 60)
    println(virtualWallet)
    println("${virtualWallet.moneyInUsd()}$")

    val realWallet = Wallet.RealWallet()
    realWallet.addMoney(RUB, 100, 8)
    realWallet.addMoney(USD, 20, 1)
    realWallet.addMoney(EUR, 10, 6)
    println(realWallet)
    println("${realWallet.moneyInUsd()}$")
    realWallet.addMoney(RUB, 100, 3)
    println(realWallet)
    println("${realWallet.moneyInUsd()}$")
}