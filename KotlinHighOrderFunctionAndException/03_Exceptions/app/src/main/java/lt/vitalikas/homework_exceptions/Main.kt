package lt.vitalikas.homework_exceptions

fun main() {
    val wheel = Wheel()

    try {
//        wheel.setPressure(-5.0)
        wheel.setPressure(6.0)
//        wheel.setPressure(2.0)
    } catch (e: Exception) {
        println(e.message)
    }
}