package lt.vitalikas.viewpager_dialogs.fragments

import android.Manifest
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import lt.vitalikas.viewpager_dialogs.databinding.FragmentOnboardingBinding
import lt.vitalikas.viewpager_dialogs.extensions.withFragments
import lt.vitalikas.viewpager_dialogs.utils.DebugLogger

class OnboardingFragment : Fragment() {
    private var _binding: FragmentOnboardingBinding? = null
    private val binding
        get() = _binding!!

    companion object {
        private const val KEY_TEXT = "text"
        private const val KEY_COLOR = "color"
        private const val KEY_DRAWABLE = "drawable"
        private const val KEY_URL = "url"

        fun newInstance(
            @StringRes textRes: Int,
            @ColorRes bgColorRes: Int,
            @DrawableRes drawableRes: Int,
            url: String
        ): OnboardingFragment {
            return OnboardingFragment().withFragments {
                putInt(KEY_TEXT, textRes)
                putInt(KEY_COLOR, bgColorRes)
                putInt(KEY_DRAWABLE, drawableRes)
                putString(KEY_URL, url)
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentOnboardingBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        requireView().setBackgroundResource(requireArguments().getInt(KEY_COLOR))
        binding.textView.setText(requireArguments().getInt(KEY_TEXT))
//        binding.imageView.setImageResource(requireArguments().getInt(KEY_DRAWABLE))

        launcher.launch(Manifest.permission.INTERNET)
    }

    private val launcher = registerForActivityResult(ActivityResultContracts.RequestPermission()) {
        if (it) {
            DebugLogger.log("perm", "granted")
            Glide.with(this)
                .load(requireArguments().getString(KEY_URL))
                .into(binding.imageView)
        } else {
            requestPermissions(
                arrayOf(
                    // INTERNET permission comes in normal protection level and that's why it's not required to ask at runtime
                    Manifest.permission.INTERNET
                ), 0
            )
        }
    }
}