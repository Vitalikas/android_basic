package lt.vitalikas.lists_recyclerview.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import com.bumptech.glide.Glide
import lt.vitalikas.lists_recyclerview.R
import lt.vitalikas.lists_recyclerview.databinding.ItemPizzaBinding
import lt.vitalikas.lists_recyclerview.databinding.ItemSushiBinding
import lt.vitalikas.lists_recyclerview.models.Food
import kotlin.properties.Delegates

class FoodAdapter(
    private val onItemClicked: (position: Int) -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private lateinit var pizzaBinding: ItemPizzaBinding
    private lateinit var sushiBinding: ItemSushiBinding

    private var foodList: List<Food> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_PIZZA -> {
                pizzaBinding =
                    ItemPizzaBinding.inflate(LayoutInflater.from(parent.context), parent, false)

                PizzaHolder(pizzaBinding, onItemClicked)
            }
            TYPE_SUSHI -> {
                sushiBinding =
                    ItemSushiBinding.inflate(LayoutInflater.from(parent.context), parent, false)

                SushiHolder(sushiBinding, onItemClicked)
            }
            else -> error("There is no holder for viewType: $viewType")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is PizzaHolder -> {
                val pizza = foodList[position].let {
                    it as? Food.Pizza
                } ?: error("Food at position = $position is not a pizza")

                holder.bind(pizza)
            }
            is SushiHolder -> {
                val sushi = foodList[position].let {
                    it as? Food.Sushi
                } ?: error("Food at position = $position is not a sushi")

                holder.bind(sushi)
            }
            else -> error("Incorrect view holder = $holder")
        }
    }

    override fun getItemCount(): Int = foodList.size

    override fun getItemViewType(position: Int): Int {
        return when (foodList[position]) {
            is Food.Pizza -> TYPE_PIZZA
            is Food.Sushi -> TYPE_SUSHI
        }
    }

    fun updateFoodList(newFoodList: List<Food>) {
        foodList = newFoodList
    }

    abstract class BaseHolder(
        binding: ViewBinding,
        onItemClicked: (position: Int) -> Unit
    ) : RecyclerView.ViewHolder(binding.root) {

        private lateinit var iconBase: ImageView
        private lateinit var nameBase: TextView
        private lateinit var priceBase: TextView
        private var drawableId by Delegates.notNull<Int>()

        init {
            when (binding) {
                is ItemPizzaBinding -> {
                    iconBase = binding.iconImageView
                    nameBase = binding.nameTextView
                    priceBase = binding.priceTextView
                    drawableId = R.drawable.ic_pizza
                }
                is ItemSushiBinding -> {
                    iconBase = binding.iconImageView
                    nameBase = binding.nameTextView
                    priceBase = binding.priceTextView
                    drawableId = R.drawable.ic_sushi
                }
            }

            binding.root.setOnClickListener {
                onItemClicked(absoluteAdapterPosition)
            }
        }

        protected fun bindBaseData(
            link: String,
            name: String,
            price: String
        ) {
            Glide.with(itemView)
                .load(link)
                .placeholder(R.drawable.ic_downloading)
                .error(drawableId)
                .into(iconBase)

            nameBase.text = name
            priceBase.text =
                itemView.context.resources.getString(R.string.price_value, price)
        }
    }

    class PizzaHolder(
        binding: ItemPizzaBinding,
        onItemClicked: (position: Int) -> Unit
    ) : BaseHolder(binding, onItemClicked) {
        private val sizeTV = binding.sizeTextView
        private val isVegetarianTV = binding.isVegetarianTextView

        fun bind(pizza: Food.Pizza) {
            bindBaseData(pizza.pictureLink, pizza.name, pizza.price.toString())
            sizeTV.text = itemView.context.resources.getString(R.string.size, pizza.size.name)
            isVegetarianTV.text =
                if (pizza.isVegetarian) itemView.context.resources.getString(R.string.vegetarian) else ""
        }
    }

    class SushiHolder(
        binding: ItemSushiBinding,
        onItemClicked: (position: Int) -> Unit
    ) : BaseHolder(binding, onItemClicked) {
        private val typeTV = binding.typeTextView

        fun bind(sushi: Food.Sushi) {
            bindBaseData(sushi.pictureLink, sushi.name, sushi.price.toString())
            typeTV.text = itemView.context.resources.getString(R.string.type, sushi.type.name)
        }
    }

    companion object {
        private const val TYPE_PIZZA = 1
        private const val TYPE_SUSHI = 2
    }
}