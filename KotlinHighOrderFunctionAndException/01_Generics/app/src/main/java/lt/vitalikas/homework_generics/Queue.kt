package lt.vitalikas.homework_generics

class Queue<T> {
    private val list = mutableListOf<T>()

    fun enqueue(item: T) = list.add(item)

    fun dequeue(): T? = list.removeFirstOrNull()

    override fun toString(): String {
        return "Queue($list)"
    }
}