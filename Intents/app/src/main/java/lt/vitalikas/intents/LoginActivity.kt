package lt.vitalikas.intents

import android.content.Intent
import android.content.res.ColorStateList
import android.os.*
import androidx.appcompat.app.AppCompatActivity
import android.util.Log
import android.util.Patterns
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.view.isGone
import androidx.core.view.isVisible
import lt.vitalikas.intents.databinding.ActivityLoginBinding

class LoginActivity : AppCompatActivity() {
    private lateinit var binding: ActivityLoginBinding
    private lateinit var progressBar: ProgressBar
    private lateinit var state: FormState

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DebugLogger.log(TAG_LIFECYCLE, "onCreate|LoginActivity|${hashCode()}")
        binding = ActivityLoginBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        progressBar = getProgressBar()

        binding.buttonLogin.setOnClickListener {
            if (isNotEmptyCredentials()) {
                val isValidEmail =
                    Patterns.EMAIL_ADDRESS.matcher(binding.inputMail.text.toString()).matches()
                if (!isValidEmail) {
                    binding.textCredentials.text = ""
                    showToast("Please enter valid email address")
                } else {
                    state = FormState(
                        true,
                        getString(R.string.login_successful, binding.inputMail.text)
                    )
                    binding.textCredentials.isVisible = false
                    makeOperations()
                }
            } else {
                state = FormState(false, getString(R.string.empty_credentials))
                updateCredentialsState()
            }
        }

        // ANR simulation
        binding.buttonSignUp.setOnClickListener {
            Thread.sleep(20000)
        }
    }

    override fun onStart() {
        super.onStart()
        DebugLogger.log(TAG_LIFECYCLE, "onStart|LoginActivity|${hashCode()}")
    }

    override fun onResume() {
        super.onResume()
        DebugLogger.log(TAG_LIFECYCLE, "onResume|LoginActivity|${hashCode()}")
    }

    override fun onPause() {
        super.onPause()
        DebugLogger.log(TAG_LIFECYCLE, "onPause|LoginActivity|${hashCode()}")
    }

    override fun onStop() {
        super.onStop()
        DebugLogger.log(TAG_LIFECYCLE, "onStop|LoginActivity|${hashCode()}")
        if (state.isValid) {
            finish()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        DebugLogger.log(TAG_LIFECYCLE, "onDestroy|LoginActivity|${hashCode()}")
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelable(KEY_CREDENTIALS, state)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        state =
            savedInstanceState.getParcelable(KEY_CREDENTIALS)
                ?: throw Exception("KEY not found")
        updateCredentialsState()
    }

    companion object {
        private const val TAG_LIFECYCLE = "activity_lifecycle"
        private const val KEY_CREDENTIALS = "credentials"
    }

    private fun updateCredentialsState() {
        if (!state.isValid) {
            binding.textCredentials.text = state.message
        }
    }

    private fun makeOperations() {
        val views =
            listOf(binding.inputMail, binding.inputPass, binding.checkboxTerms, binding.buttonLogin)

        views.forEach { it.isEnabled = false }

        binding.constraintLayout.addView(progressBar)

        constraintView()

        Handler(Looper.getMainLooper()).postDelayed({
            progressBar.isGone = true

            views.forEach { it.isEnabled = true }

            showToast(state.message)

            launchMainActivity()
        }, 2000)
    }

    private fun launchMainActivity() {
        val mainActivityIntent = Intent(
            this, MainActivity::class.java
        )
        startActivity(mainActivityIntent)
    }

    private fun constraintView() {
        val params = progressBar.layoutParams as ConstraintLayout.LayoutParams
        params.leftToLeft = binding.guidelineVerticalStart.id
        params.rightToRight = binding.guidelineVerticalEnd.id
        params.topToBottom = binding.checkboxTerms.id
        params.topMargin = 8
        progressBar.requestLayout()
    }

    private fun getProgressBar(): ProgressBar {
        return ProgressBar(this).apply {
            indeterminateTintList =
                ColorStateList.valueOf(ContextCompat.getColor(this@LoginActivity, R.color.jalapeno))
        }
    }

    private fun isNotEmptyCredentials() =
        binding.inputMail.text.toString().isNotEmpty() && binding.inputPass.text.toString()
            .isNotEmpty()

    private fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    private object DebugLogger {
        fun log(tag: String, message: String) {
            if (BuildConfig.DEBUG) {
                Log.d(tag, message)
            }
        }
    }
}