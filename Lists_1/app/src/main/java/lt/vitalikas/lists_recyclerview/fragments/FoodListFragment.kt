package lt.vitalikas.lists_recyclerview.fragments

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import by.kirich1409.viewbindingdelegate.viewBinding
import lt.vitalikas.lists_recyclerview.R
import lt.vitalikas.lists_recyclerview.adapters.FoodAdapter
import lt.vitalikas.lists_recyclerview.databinding.FragmentFoodListBinding
import lt.vitalikas.lists_recyclerview.models.*
import lt.vitalikas.lists_recyclerview.utils.AutoClearedValue

class FoodListFragment : Fragment(R.layout.fragment_food_list), Receiver {
    // ViewBinding Property Delegate for viewBinding and
    // auto-clearing of views on fragment`s view destroy
    private val binding by viewBinding(FragmentFoodListBinding::bind)

    private var foodList = listOf(
        Food.Pizza(
            "Tomatoes",
            "https://image.flaticon.com/icons/png/512/432/432339.png",
            6.40,
            Food.Pizza.Size.MEDIUM,
            true
        ),
        Food.Sushi(
            "Tako",
            "https://image.flaticon.com/icons/png/512/4977/4977756.png",
            9.70,
            Food.Sushi.Type.SASHIMI
        ),
        Food.Sushi(
            "Ebi",
            "https://image.flaticon.com/icons/png/512/4977/4977908.png",
            8.50,
            Food.Sushi.Type.SASHIMI
        ),
        Food.Sushi(
            "Futomaki",
            "https://image.flaticon.com/icons/png/512/4978/4978029.png",
            7.30,
            Food.Sushi.Type.MAKI
        )
    ).shuffled()

    // Lifecycle API for auto-clearing of values on fragment`s view destroy
    private var foodAdapter by AutoClearedValue<FoodAdapter>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initRecyclerView(binding.foodListRecyclerView)

        if (savedInstanceState != null) {
            foodList = savedInstanceState.getParcelableArrayList(FOOD_LIST)
                ?: error(getString(R.string.error_bundle))
            toggleListInfoText(foodList)
        }

        foodAdapter.updateFoodList(foodList)
        foodAdapter.notifyDataSetChanged()

        binding.addFab.setOnClickListener {
            showDialog()
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        outState.putParcelableArrayList(FOOD_LIST, foodList as ArrayList)
    }

    private fun showDialog() {
        DataDialogFragment().show(childFragmentManager, getString(R.string.tag_dialog))
    }

    private fun addFoodToList(food: Food) {
        foodList = listOf(food) + foodList

        foodAdapter.updateFoodList(foodList)
        foodAdapter.notifyItemInserted(0)
        binding.foodListRecyclerView.scrollToPosition(0)

        toggleListInfoText(foodList)
    }

    private fun deleteFood(position: Int) {
        foodList = foodList.filterIndexed { index, _ -> index != position }

        foodAdapter.updateFoodList(foodList)
        foodAdapter.notifyItemRemoved(position)

        toggleListInfoText(foodList)
    }

    private fun toggleListInfoText(foodList: List<Food>) {
        binding.listInfoTextView.isVisible = foodList.isEmpty()
    }

    private fun initRecyclerView(foodRecyclerView: RecyclerView) {
        foodAdapter = FoodAdapter { position -> deleteFood(position) }

        with(foodRecyclerView) {
            adapter = foodAdapter
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
        }
    }

    override fun onFoodReceivedCallback(food: Food) {
        addFoodToList(food)
    }

    companion object {
        private const val FOOD_LIST = "food"
    }
}