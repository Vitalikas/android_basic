package lt.vitalikas.intents

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class FormState(
        var isValid: Boolean,
        var message: String
) : Parcelable