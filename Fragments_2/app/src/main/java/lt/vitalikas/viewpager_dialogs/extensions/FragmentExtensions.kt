package lt.vitalikas.viewpager_dialogs.extensions

import android.os.Bundle
import androidx.fragment.app.Fragment

fun <T : Fragment> T.withFragments(action: Bundle.() -> Unit): T {
    return apply {
        val args = Bundle().apply(action)
        arguments = args
    }
}