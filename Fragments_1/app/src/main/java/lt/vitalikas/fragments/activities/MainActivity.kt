package lt.vitalikas.fragments.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import lt.vitalikas.fragments.fragments.Navigator
import lt.vitalikas.fragments.fragments.LoginFragment
import lt.vitalikas.fragments.R
import lt.vitalikas.fragments.databinding.ActivityMainBinding
import lt.vitalikas.fragments.utils.DebugLogger

class MainActivity : AppCompatActivity(), Navigator {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        if (savedInstanceState == null) {
            addFragment()
        }
    }

    // create LoginFragment
    private fun addFragment() {
        supportFragmentManager
            .beginTransaction()
            .add(R.id.fragmentContainer, LoginFragment(), getString(R.string.tag_login))
            .commit()
    }

    // navigate(replace) to MainFragment
    override fun navigateTo(fragment: Fragment) {
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragmentContainer, fragment, getString(R.string.tag_main))
            .commit()
    }

    override fun onBackPressed() {
        supportFragmentManager
            .findFragmentByTag(getString(R.string.tag_main))?.let { fragment ->
                fragment.childFragmentManager.takeIf {
                    DebugLogger.log("fragment_stack", it.backStackEntryCount.toString())
                    it.backStackEntryCount > 0
                }?.popBackStack() ?: super.onBackPressed()
            }
    }
}