package lt.vitalikas.viewpager_dialogs.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import lt.vitalikas.viewpager_dialogs.R
import lt.vitalikas.viewpager_dialogs.adapters.OnboardingAdapter
import lt.vitalikas.viewpager_dialogs.databinding.ActivitySliderBinding
import lt.vitalikas.viewpager_dialogs.models.OnboardingScreen

class SliderActivity : AppCompatActivity() {
    private lateinit var binding: ActivitySliderBinding

    private val screens = listOf(
        OnboardingScreen(
            R.string.onboarding_text_1,
            R.color.onboarding_color_1,
            R.drawable.onboarding_1,
            "https://image.flaticon.com/icons/png/128/3595/3595455.png"
        ),
        OnboardingScreen(
            R.string.onboarding_text_2,
            R.color.onboarding_color_2,
            R.drawable.onboarding_1,
            "https://image.flaticon.com/icons/png/512/3595/3595455.png"
        ),
        OnboardingScreen(
            R.string.onboarding_text_3,
            R.color.onboarding_color_3,
            R.drawable.onboarding_1,
            "https://image.flaticon.com/icons/png/512/3595/3595455.png"
        ),
        OnboardingScreen(
            R.string.onboarding_text_4,
            R.color.onboarding_color_4,
            R.drawable.onboarding_1,
            "https://image.flaticon.com/icons/png/512/3595/3595455.png"
        ),
        OnboardingScreen(
            R.string.onboarding_text_5,
            R.color.onboarding_color_5,
            R.drawable.onboarding_1,
            "https://image.flaticon.com/icons/png/512/3595/3595455.png"
        )
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivitySliderBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        val adapter = OnboardingAdapter(this, screens)
        binding.viewPager.adapter = adapter
        binding.viewPager.offscreenPageLimit = 1

        binding.dotsIndicator.setViewPager2(binding.viewPager)

        // animation transform
        binding.viewPager.setPageTransformer { page, position ->
            when {
                position < -1 || position > 1 -> {
                    page.alpha = 0f
                }
                position <= 0 -> {
                    page.alpha = 1 + position
                }
                position <= 1 -> {
                    page.alpha = 1 - position
                }
            }
        }
    }
}