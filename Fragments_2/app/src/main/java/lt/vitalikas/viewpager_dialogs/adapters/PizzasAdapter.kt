package lt.vitalikas.viewpager_dialogs.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import lt.vitalikas.viewpager_dialogs.models.Pizza
import lt.vitalikas.viewpager_dialogs.fragments.PizzaFragment

class PizzasAdapter(activity: FragmentActivity, private val pizzas: List<Pizza>) :
    FragmentStateAdapter(activity) {
    override fun getItemCount(): Int {
        return pizzas.size
    }

    override fun createFragment(position: Int): Fragment {
        val pizza = pizzas[position]

        return PizzaFragment.newInstance(pizza.titleRes, pizza.descriptionRes, pizza.type)
    }
}